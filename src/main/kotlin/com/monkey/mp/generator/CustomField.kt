package com.monkey.mp.generator

/**
 * 作者：Monkey
 * 日期：2017/9/26
 */
data class CustomField(
    var import: String?,
    var content: String?
)